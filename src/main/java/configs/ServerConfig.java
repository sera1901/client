package configs;

import common.http.Server;
import common.http.Servers;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.conn.routing.HttpRoute;

public class ServerConfig implements Server {

    private String host;
    private String scheme = "https";
    private int port  = 443;
    private Header[] headers;


    public ServerConfig(String host, Header[] headers){
        this.host = host;
        this.headers = headers;

        Servers.setConnectionManager(getRoute(), 2);
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public String getScheme() {
        return scheme;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public HttpRoute getRoute() {
        return  new HttpRoute(new HttpHost(getHost(), getPort(), getScheme()));
    }

    @Override
    public Header[] getHeaders() {
        return headers;
    }

    @Override
    public String getPath() {
        return null;
    }
}
