package repository;

import common.Response;
import common.databases.Database;
import common.repository.AbstractRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CompanyRepository extends AbstractRepository {
    private static final Logger logger = LoggerFactory.getLogger(CompanyRepository.class);

    private Database db;
    private final String table = "company";

    public CompanyRepository(Database db) {
        super(logger);
        this.db = db;
    }


    public Response createCompany(String name, String address, String country, String state,String city, String postal, String siteUrl){
        long res = this.db.builder().table(table)
                .columnsBuilder()
                .columnValue("name", name)
                .columnValue("address", address)
                .columnValue("country", country)
                .columnValue("state", state)
                .columnValue("city", city)
                .columnValue("postal",postal)
                .columnValue("site_url", siteUrl)
                .build().insert(false);

        return super.response(res,
                "Successfully created company",
                "Could not create company");
    }
}
