package repository;

import common.Response;
import common.http.HttpRequest;
import common.repository.HttpRepository;

public class RemoteInstanceRepository extends HttpRepository {

    private final HttpRequest server;

    public RemoteInstanceRepository(HttpRequest server) {
        this.server = server;
    }


    public Response createClient(String instanceId) {
        return response(
                server.builder()
                        .path("/client/" + instanceId)
                        .body("{}")
                        .post()
        );
    }

}
