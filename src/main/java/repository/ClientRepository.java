package repository;


import common.Response;
import common.databases.Database;
import common.repository.AbstractRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientRepository extends AbstractRepository {

    private static final Logger logger = LoggerFactory.getLogger(ClientRepository.class);
    private Database db;
    private String table = "client";

    public ClientRepository(Database db) {
        super(logger);
        this.db = db;
    }


    public Response createClient(String email, String firstname, String lastname, String phone, String password){
        long response = this.db.builder().table(table).columnsBuilder()
                .columnValue("email", email)
                .columnValue("firstname", firstname)
                .columnValue("lastname", lastname)
                .columnValue("phone", phone)
                .columnValue("password", password)
                .build().insert(false);

        return super.response(response,
                "Successfully created client",
                "Could not create client");
    }






}
