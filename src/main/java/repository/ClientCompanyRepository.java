package repository;

import common.Response;
import common.databases.Database;
import common.repository.AbstractRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientCompanyRepository extends AbstractRepository {
    private static final Logger logger = LoggerFactory.getLogger(ClientCompanyRepository.class);

    private Database db;
    private final String table = "client_companies";

    public ClientCompanyRepository(Database db) {
        super(logger);
        this.db = db;
    }

    public Response addCompanyToClient(String clientEmail, Long companyId){
        return addCompanyToClient(clientEmail, companyId, "ADMIN");
    }


    public Response addCompanyToClient(String clientEmail, Long companyId, String type){
        long res = this.db.builder().table(table)
                .columnsBuilder()
                .columnValue("client_email", clientEmail)
                .columnValue("company_id", companyId)
                .columnValue("type", type)
                .build().insert(false);

        return super.response(res,
                "Successfully added company to client",
                "Could not add company to client");
    }
}
