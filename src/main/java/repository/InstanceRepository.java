package repository;


import common.Response;
import common.databases.Database;
import common.repository.AbstractRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static common.repository.Format.form;

public class InstanceRepository extends AbstractRepository {

    private static final Logger logger = LoggerFactory.getLogger(InstanceRepository.class);
    private Database db;
    private String table = "instance";

    public InstanceRepository(Database db) {
        super(logger);
        this.db = db;
    }


    public Response createInstance(String name, Long subscriptionId, Long companyId, String expiryDate, String apiUrl, String dashboardUrl){
        long response = this.db.builder().table(table).columnsBuilder()
                .columnValue("name", name)
                .columnValue("subscription_id", subscriptionId)
                .columnValue("company_id", companyId)
                .columnValue("expiry_date", expiryDate)
                .columnValue("api_url", apiUrl)
                .columnValue("dashboard_url", dashboardUrl)
                .build().insert(false);

        return super.response(response,
                "Successfully created instance",
                "Could not create instance");
    }



    public Response setStatus(String name, String status){
        long response = this.db.builder().table(table)
                .columnsBuilder().columnValue("status", status)
                .build().limit(1)
                .conditions("name="+form(name))
                .update();


        return response(response,
                "Successfully updated instance status",
                "Could not update instance status");
    }






}
