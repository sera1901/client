package domain.controllers;

import common.DateUtil;
import common.EmailSender;
import common.Response;
import domain.objects.Instance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import repository.InstanceRepository;
import repository.RemoteInstanceRepository;
import utils.Random;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.HashMap;
import java.util.Map;

import static app.Constants.*;
import static common.Response.State.SUCCESS;
import static java.time.temporal.ChronoUnit.MONTHS;
import static java.time.temporal.ChronoUnit.YEARS;
import static lib.Constants.TIMEZONE;

@Component
@RestController
@CrossOrigin
@RequestMapping("/instances")
public class InstanceController {

    private InstanceRepository instanceRepository;
    private RemoteInstanceRepository remoteInstanceRepository;
    private EmailSender sender;

    @Autowired
    public InstanceController(InstanceRepository instanceRepository, RemoteInstanceRepository remoteInstanceRepository, @Nullable EmailSender sender) {
        this.instanceRepository = instanceRepository;
        this.remoteInstanceRepository = remoteInstanceRepository;
        this.sender = sender;
    }

    @PostMapping("/test/{email}")
    public Response createTestInstance(@PathVariable String email, @RequestBody Instance instance){

        String id =  Random.generateRandomName(instance.getName());
        HashMap<String, Object> json = new HashMap<>();

        Response res = instanceRepository.createInstance(
                id,
                1L,
                instance.getCompanyId(),
                DateUtil.now(TIMEZONE).plus(1, YEARS).toString(),
                API_URL+id,
                DASHBOARD_URL+id
        );

        json.put("instanceId", id);
        json.put("creation", res);

        try {
            (new InternetAddress(email)).validate();

            if(res.state == SUCCESS){
                res = this.remoteInstanceRepository.createClient(id);
                json.put("deployment", res);

                if(res.state == SUCCESS){
                    sendTestInstanceEmail(email, id);
                    return Response.success("Successfully launched instance", json);
                }else{
                    res = this.instanceRepository.setStatus(id,"FAILED");
                    json.put("failed", res);
                    return Response.fail("Could not launch instance", json);
                }
            }else{
                return Response.fail("Could not add instance", res.result);
            }

        } catch (AddressException e) {
            return Response.fail("Your email Address is invalid", email);
        }

    }

    @PostMapping("/email/test")
    public Response createTestInstance(){

        sendTestInstanceEmail("adri.sermax@gmail.com", "jevaisbienzavhb7j");

        return Response.success("Successfully send email", "adri.sermax@gmail.com");
    }


    private void sendTestInstanceEmail(String email, String id){
        Map<String, Object> model = new HashMap<>();

        model.put("downloadWindow10", DOWNLOAD_POS_WINDOW10_URL);
        model.put("dashboard", DASHBOARD_URL+id);
        model.put("api", API_URL+id);

        this.sender.sendTemplate("emails/welcome.pug", "Version test: O-Possum Solution Inc.", model, email);
    }
}
