package domain.controllers;

import common.Encryption;
import common.Response;
import domain.objects.Client;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import repository.ClientCompanyRepository;
import repository.ClientRepository;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

@Component
@RestController
@RequestMapping("/clients")
@CrossOrigin
public class ClientController {

    private ClientRepository clientRepository;
    private ClientCompanyRepository clientCompanyRepository;

    @Autowired
    public ClientController(ClientRepository clientRepository, ClientCompanyRepository clientCompanyRepository) {
        this.clientRepository = clientRepository;
        this.clientCompanyRepository = clientCompanyRepository;
    }

    @PostMapping("")
    public Response createClient(@RequestBody Client client){
        try {
            (new InternetAddress(client.getEmail())).validate();

            return clientRepository.createClient(
                    client.getEmail(),
                    client.getFirstname(),
                    client.getLastname(),
                    client.getPhone(),
                    client.getPassword()
            );

        } catch (AddressException e) {
            return Response.fail("Your email Address is invalid", client);
        }

    }

    @PostMapping("/test")
    public Response createClientTest(@RequestBody Client client){
        try {
            (new InternetAddress(client.getEmail())).validate();

            return clientRepository.createClient(
                    client.getEmail(),
                    client.getFirstname(),
                    client.getLastname(),
                    client.getPhone(),
                    Encryption.encrypt(RandomStringUtils.random(10).toCharArray())
            );

        } catch (AddressException e) {
            return Response.fail("Your email Address is invalid", client);
        }
    }

    @PostMapping("/test/{email}/{companyId}")
    public Response addClientTest(@PathVariable String  email, @PathVariable Long companyId){
        try {
            (new InternetAddress(email)).validate();

            return clientCompanyRepository.addCompanyToClient(
                    email,
                    companyId
            );

        } catch (AddressException e) {
            return Response.fail("Your email Address is invalid", email);
        }
    }
}
