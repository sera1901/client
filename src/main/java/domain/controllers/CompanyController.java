package domain.controllers;

import common.Response;
import domain.objects.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import repository.CompanyRepository;

@Component
@RestController
@RequestMapping("/companies")
@CrossOrigin
public class CompanyController {

    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyController(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @PostMapping("")
    public Response createCompany(@RequestBody Company company) {
        return companyRepository.createCompany(
                company.getName(),
                company.getAddress(),
                company.getCountry(),
                company.getState(),
                company.getCity(),
                company.getPostal(),
                company.getSiteUrl()
        );
    }
}
