package app;

public class Constants {
    static final String EMAIL_PASSWORD = System.getenv("EMAIL_PASSWORD");
    static final String EMAIL = System.getenv("EMAIL");
    static final String EMAIL_HOST = System.getenv("EMAIL_HOST");

    static final public String DASHBOARD_URL = System.getenv("DASHBOARD_URL");
    static final public String API_URL = System.getenv("API_URL");
    static final public String DOWNLOAD_POS_WINDOW10_URL = System.getenv("DOWNLOAD_POS_WINDOW10_URL");
}
