package app;

import common.EmailSender;
import common.http.ApacheHttpRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import repository.*;

import static app.Constants.*;

@Configuration
public class DataConfig {

    @Bean
    public ClientRepository clientRepository(){
        return new ClientRepository(DataLoader.db);
    }

    @Bean
    public CompanyRepository companyRepository(){
        return new CompanyRepository(DataLoader.db);
    }

    @Bean
    public ClientCompanyRepository clientCompanyRepository(){
        return new ClientCompanyRepository(DataLoader.db);
    }

    @Bean
    public InstanceRepository instanceRepository(){
        return new InstanceRepository(DataLoader.db);
    }

    @Bean
    public EmailSender emailSender(){
        if (EMAIL_PASSWORD != null && EMAIL != null && EMAIL_HOST != null){
            System.out.println(EMAIL);
            return new EmailSender(EMAIL_HOST, EMAIL, EMAIL_PASSWORD,null);
        }

        return null;
    }


    @Bean
    public RemoteInstanceRepository remoteInstanceRepository(){
        return new RemoteInstanceRepository(
                new ApacheHttpRequest(
                        DataLoader.instanceServer
                )
        );
    }
}
