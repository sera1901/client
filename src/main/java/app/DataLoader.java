package app;

import common.databases.Database;
import common.databases.MySQLDatabase;
import common.http.Server;
import configs.ServerConfig;
import org.apache.http.message.BasicHeader;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;

public class DataLoader {

     static Server instanceServer;
     static Database db;


     static{
         boolean res;
          res = loadDb();
         res = res && loadInstanceServer();
         if(!res){
             System.exit(1);
         }
     }

     private static boolean loadDb(){
         String url = System.getenv("CLIENT_DB_URL");
         String user = System.getenv("CLIENT_DB_USER");
         String password = System.getenv("CLIENT_DB_PASSWORD");

         boolean res;

         res = check("CLIENT_DB_URL", url);
         res = res && check("CLIENT_DB_USER", user);
         res = res && check("CLIENT_DB_PASSWORD", password);


         if(res){
             db = loadDatabase("Client Database", url, user, password, 5, true, "client/migration",3);
         }

         return res;
     }

     private static boolean loadInstanceServer(){
         String host = System.getenv("INSTANCE_SERVER_HOST");
         String token = System.getenv("INSTANCE_SERVER_TOKEN");

         boolean res = check("INSTANCE_SERVER_HOST", host);
         res = res && check("INSTANCE_SERVER_TOKEN", token);


         if(res){
             instanceServer = new ServerConfig(
                     host,
                     new BasicHeader[]{
                             new BasicHeader("Authorization",token)
                     }
             );
         }

         return res;
     }

     private static boolean check(String name, String var){
         if(var == null || var.equals("")){
             System.out.println(name +" is missing");
             return false;
         }
         return true;
     }



    private static Database loadDatabase(String name, String url, String user, String pass, int poolSize, boolean breaking, String migrationPath, int retry){

        Database db = null;
        try {
            db = new MySQLDatabase(poolSize);
            db.connect(url, user, pass);

            if(migrationPath != null){
                Flyway flyway = Flyway.configure().baselineOnMigrate(true).locations(migrationPath)
                        .dataSource("jdbc:mysql://"+url+"?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=US/Eastern", user, pass)
                        .load();
                flyway.repair();
                flyway.migrate();
            }

        }catch(FlywayException e){
            System.err.println("Could not run migration: " + e.getMessage());
        }catch(RuntimeException e){
            System.out.println("OOPS, could not connect to "+name+" database. Please rebuild and make sure you have " +
                    "the database up and running and that you have the right credentials.");
            if(retry > 0){
                System.err.println("Retrying in 10s");
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                db = loadDatabase(name, url, user,pass,poolSize,breaking,migrationPath,--retry);
            }else if(breaking) {
                System.exit(0);
            }
        }

        return db;
    }


}
