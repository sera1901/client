package utils;

import org.apache.commons.lang3.RandomStringUtils;

public class Random {
    public static String generateRandomName(String name){
        return name.replaceAll("[^a-zA-Z0-9]", "").toLowerCase()
                + RandomStringUtils.random(7, "abcdefghijkpqrstuvwxyz23456789");
    }
}
