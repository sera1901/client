-- MySQL Script generated by MySQL Workbench
-- Mon Jan 18 22:28:06 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema opossum
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema opossum
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `opossum` DEFAULT CHARACTER SET utf8 ;
USE `opossum` ;

-- -----------------------------------------------------
-- Table `opossum`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opossum`.`client` (
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `firstname` VARCHAR(255) NULL,
  `lastname` VARCHAR(255) NULL,
  `phone` VARCHAR(255) NULL,
  `registered_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` VARCHAR(45) NOT NULL DEFAULT 'READY',
  PRIMARY KEY (`email`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `opossum`.`payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opossum`.`payment` (
  `payment_id` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `payment_profile_id` VARCHAR(255) NOT NULL,
  `type` VARCHAR(45) NOT NULL DEFAULT 'PAYSAFE',
  `status` VARCHAR(45) NOT NULL DEFAULT 'READY',
  UNIQUE INDEX `payment_id_UNIQUE` (`payment_id` ASC) VISIBLE,
  PRIMARY KEY (`payment_id`),
  CONSTRAINT `payment_profile_id`
    FOREIGN KEY (`email`)
    REFERENCES `opossum`.`client` (`email`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `opossum`.`company`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opossum`.`company` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `payment_id` VARCHAR(255) NULL,
  `address` VARCHAR(255) NULL,
  `country` VARCHAR(255) NULL,
  `state` VARCHAR(255) NULL,
  `city` VARCHAR(255) NULL,
  `postal` VARCHAR(255) NULL,
  `site_url` VARCHAR(255) NULL,
  `status` VARCHAR(45) NOT NULL DEFAULT 'READY',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idcompany_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `payment_fk_idx` (`payment_id` ASC) VISIBLE,
  CONSTRAINT `payment_fk`
    FOREIGN KEY (`payment_id`)
    REFERENCES `opossum`.`payment` (`payment_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `opossum`.`client_companies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opossum`.`client_companies` (
  `client_email` VARCHAR(255) NOT NULL,
  `company_id` BIGINT NOT NULL,
  `type` VARCHAR(45) NOT NULL DEFAULT 'ADMIN',
  `created_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_email`, `company_id`),
  INDEX `company_fk_idx` (`company_id` ASC) VISIBLE,
  CONSTRAINT `client_fk`
    FOREIGN KEY (`client_email`)
    REFERENCES `opossum`.`client` (`email`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `company_fk`
    FOREIGN KEY (`company_id`)
    REFERENCES `opossum`.`company` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `opossum`.`subscription`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opossum`.`subscription` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `price` DOUBLE NOT NULL DEFAULT 0,
  `type` VARCHAR(45) NOT NULL DEFAULT 'FREE TEST',
  `description` VARCHAR(255) NULL,
  `status` VARCHAR(45) NOT NULL DEFAULT 'ACTIVE',
  `instance_limit` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB;


LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
INSERT INTO `subscription` VALUES (1,0,'FREE TEST','FREE','ACTIVE',1);
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;

-- -----------------------------------------------------
-- Table `opossum`.`instance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opossum`.`instance` (
  `subscription_id` BIGINT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `company_id` BIGINT NOT NULL,
  `expiry_date` DATETIME NOT NULL,
  `status` VARCHAR(45) NOT NULL DEFAULT 'ACTIVE',
  `api_url` VARCHAR(255) NULL,
  `dashboard_url` VARCHAR(255) NULL,
  PRIMARY KEY (`subscription_id`, `company_id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
  INDEX `company_fk_idx` (`company_id` ASC) INVISIBLE,
  CONSTRAINT `company_fk2`
    FOREIGN KEY (`company_id`)
    REFERENCES `opossum`.`company` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `subscription_fk`
    FOREIGN KEY (`subscription_id`)
    REFERENCES `opossum`.`subscription` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `opossum`.`invoice`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opossum`.`invoice` (
  `id` BIGINT NOT NULL,
  `type` VARCHAR(255) NOT NULL,
  `subscription` BIGINT NOT NULL,
  `barcode` VARCHAR(255) NOT NULL,
  `company_id` BIGINT NOT NULL,
  `status` VARCHAR(45) NOT NULL DEFAULT 'PENDING',
  `total` DOUBLE NOT NULL DEFAULT 0,
  `meta` JSON NULL,
  PRIMARY KEY (`id`, `type`),
  INDEX `subscription_fk_idx` (`subscription` ASC) VISIBLE,
  INDEX `company_fk_idx` (`company_id` ASC) VISIBLE,
  CONSTRAINT `subscription_fk2`
    FOREIGN KEY (`subscription`)
    REFERENCES `opossum`.`subscription` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `company_fk3`
    FOREIGN KEY (`company_id`)
    REFERENCES `opossum`.`company` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `opossum`.`transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `opossum`.`transaction` (
  `id` BIGINT NOT NULL,
  `type` VARCHAR(255) NOT NULL,
  `invoice_id` BIGINT NOT NULL,
  `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` VARCHAR(45) NOT NULL DEFAULT 'PENDING',
  `amount` DOUBLE NOT NULL DEFAULT 0,
  `payment_type` VARCHAR(255) NOT NULL DEFAULT 'PAYSAFE',
  PRIMARY KEY (`id`, `type`),
  CONSTRAINT `invoice_pk`
    FOREIGN KEY (`id` , `type`)
    REFERENCES `opossum`.`invoice` (`id` , `type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


--
-- Table structure for table `flyway_schema_history`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
SET character_set_client = utf8mb4 ;
CREATE TABLE `flyway_schema_history` (
                                         `installed_rank` int(11) NOT NULL,
                                         `version` varchar(50) DEFAULT NULL,
                                         `description` VARCHAR(255) NOT NULL,
                                         `type` varchar(20) NOT NULL,
                                         `script` varchar(1000) NOT NULL,
                                         `checksum` int(11) DEFAULT NULL,
                                         `installed_by` VARCHAR(255) NOT NULL,
                                         `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                         `execution_time` int(11) NOT NULL,
                                         `success` tinyint(1) NOT NULL,
                                         PRIMARY KEY (`installed_rank`),
                                         KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
