package app;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import java.net.URL;

class GenerateId {

    @Test
    public void givenUsingApache_whenGeneratingRandomAlphanumericString_thenCorrect() {

        String generatedString = RandomStringUtils.random(6, "abcdefghijkpqrstuvwABCDEFGHJKLPRSTUVWJKP23456789");

        System.err.println("here is the string: "+generatedString);
    }

    @Test
    public void givenUsingApache_whenGeneratingRandomAlphabeticString_thenCorrect() {
        String generatedString = RandomStringUtils.randomAlphabetic(10);

        System.out.println(generatedString);
    }

    @Test
    public void JavaKeepOnlyLetterAndNumbersExample() {
        //keep only letters and numbers (alphanumeric)
        String str = "228, Park Avenue, Building no # 5 o-possum";
        str = str.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
        System.out.println(str);
    }

    @Test
    public void TestPath() {
        URL url = this.getClass().getClassLoader().getResource("emails/welcome.pug");
        if (url == null) {
            System.err.println("PATH NOT OK !!!!!!!!!!!");
        }else {
            System.err.println("PATH OK :)");
        }
    }

}
